package com.example.android.peterboroughtourguide;

import java.util.ArrayList;

/**
 * Created by mustaphar on 3/15/17.
 */

public class BarsClubsData {
    ArrayList<BarsClubsModel> data = new ArrayList<>();

    BarsClubsData() {
        String hrs[] = new String[7];
        hrs[0] = "0000 - 1200";
        hrs[1] = "0000 - 1200";
        hrs[3] = "0000 - 1200";
        hrs[4] = "0000 - 1230";
        hrs[5] = "0000 - 1200";
        hrs[6] = "1300 - 1700";
        data.add(new BarsClubsModel("The Stoneworks Bar",
                "8B Church St, Peterborough, UK",
                "PE1 1XB",
                "missing",
                hrs,
                4.6f));

        hrs = new String[7];
        hrs[0] = "Closed";
        hrs[1] = "1200 - 2330";
        hrs[2] = "1200 - 2330";
        hrs[3] = "1200 - 2330";
        hrs[4] = "1200 - 2330";
        hrs[5] = "1200 - 2330";
        hrs[6] = "1200 - 2330";
        data.add(new BarsClubsModel("Hub's Place",
                "No.1, 12 Market Street, Whittlesey, UK",
                "PE7 1AB",
                "+44 1733 204199",
                hrs,
                3.8f));

        hrs = new String[7];
        hrs[0] = "1200 - 0400";
        hrs[1] = "1200 - 0400";
        hrs[2] = "1200 - 0400";
        hrs[3] = "1200 - 0400";
        hrs[4] = "1200 - 0400";
        hrs[5] = "1200 - 0400";
        hrs[6] = "1200 - 0400";
        data.add(new BarsClubsModel("The New Crown",
                "58 High Causeway, Whittlesey, Peterborough PE7 1QA, UK",
                "PE7 1QA",
                "+44 1733 205134",
                hrs,
                3.5f));
    }

    public ArrayList<BarsClubsModel> getData() {
        return data;
    }
}
