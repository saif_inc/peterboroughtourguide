package com.example.android.peterboroughtourguide;

/**
 * Created by mustaphar on 3/15/17.
 */

public class BarsClubsModel {

    String name;
    String address;
    String postCode;
    String number;
    String[] hrs = new String[7];
    float rating;

    BarsClubsModel(String name, String address, String postCode, String number, String[] hrs, float rating) {
        this.name = name;
        this.address = address;
        this.postCode = postCode;
        this.number = number;
        this.hrs = hrs;
        this.rating = rating;
    }

    public String getAddress() {
        return address;
    }

    public String[] getHrs() {
        return hrs;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public String getPostCode() {
        return postCode;
    }

    public float getRating() {
        return rating;
    }
}
