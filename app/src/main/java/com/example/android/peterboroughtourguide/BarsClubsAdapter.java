package com.example.android.peterboroughtourguide;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by mustaphar on 3/15/17.
 */

public class BarsClubsAdapter extends ArrayAdapter<BarsClubsModel> {
    public BarsClubsAdapter(AppCompatActivity activity, ArrayList<BarsClubsModel> data) {
        super(activity, 0, data);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }

        BarsClubsModel model = getItem(position);

        TextView tvName = (TextView) listItemView.findViewById(R.id.name);
        tvName.setText(model.getName());

        TextView tvPostCode = (TextView) listItemView.findViewById(R.id.post_code);
        tvPostCode.setText(model.getPostCode());

        RatingBar rating = (RatingBar) listItemView.findViewById(R.id.rating);
        rating.setRating(model.getRating());


        return listItemView;
    }
}
